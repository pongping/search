<?php
/**
 * Search plugin for Craft CMS 3.x
 *
 * Makes lazy loading Ajax search available
 *
 * @link      https://pongping.studio
 * @copyright Copyright (c) 2019 PongPing
 */

/**
 * Search en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('search', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    PongPing
 * @package   Search
 * @since     1.0.0
 */
return [
    'Search plugin loaded' => 'Search plugin loaded',
];
