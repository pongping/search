<?php
/**
 * Search plugin for Craft CMS 3.x
 *
 * Makes lazy loading Ajax search available
 *
 * @link      https://pongping.studio
 * @copyright Copyright (c) 2019 PongPing
 */

namespace pongping\search\controllers;

use craft\db\Query;
use pongping\search\Search;

use Craft;
use craft\web\Controller;
use craft\services;
use craft\elements\Entry;
use craft\web\View;


/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    PongPing
 * @package   Search
 * @since     1.0.0
 */
class DefaultController extends Controller
{

	private $returnData = [];

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'lazy-loading', 'search'];

    // Public Methods
    // =========================================================================

	/**
	 * @return mixed
	 */
    public function actionLazyLoading(){
	    $result = 'Welcome to the DefaultController actionLazyLoading() method';
	    return $result;
    }

	/**
	 * @return mixed
	 */
    public function actionSearch(){
    	$request = Craft::$app->getRequest();

	    $data = $request->getParam('data');

	    if ($data && strlen($data)){
		    $dataArray = json_decode(stripslashes($data));

		    $reset = $dataArray->reset;

		    if ($reset){
		    	$this->_resetSearch($dataArray);
		    } else {
		    	$this->_search($dataArray);
		    }

		    return $this->asJson($this->returnData);
	    }

	    return $this->asJson(false);
    }

    private function _getFilterTagHtml($fields, $dataType) {
    	$filterTagsHtml = '';

	    foreach ($fields as &$field) {
		    $fieldType = $field->type;
		    $fieldHandle = $field->handle;

		    if ($fieldType == 'number-slider') {
			    $filterTagsHtml .= \Craft::$app->view->renderTemplate('_includes/filter/filterTag',
				    array(
					    'dataType' => $dataType,
					    'fieldHandle' => $fieldHandle,
					    'fieldType' => $fieldType,
					    'values' => $field->values
				    )
			    );
		    } else {
			    foreach ($field->values as &$value) {
				    $filterTagsHtml .= \Craft::$app->view->renderTemplate('_includes/filter/filterTag',
					    array(
						    'dataType' => $dataType,
						    'fieldHandle' => $fieldHandle,
						    'fieldType' => $fieldType,
						    'value' => $value
					    )
				    );
			    }
		    }
	    }
	    return $filterTagsHtml;
    }

    private function _resetSearch($dataArray) {
	    $section = $dataArray->section;
	    $numberOfItems = $dataArray->numberOfItems;
	    $pageNr = $dataArray->pageNr;

	    $offset = $this->_getOffset($pageNr, $numberOfItems);

	    $query = Entry::find()
		    ->section($section)
		    ->limit($numberOfItems)
		    ->offset($offset);

	    $count = $query->count();
	    $entries = $query->all();

	    $this->returnData['filterTagsHtml'] = '';
	    $this->returnData['total'] = $count;

	    if ($count == 0) {
		    $this->returnData['html'] = \Craft::$app->view->renderTemplate('_includes/filter/noResults');
		    $this->returnData['total'] = 0;
	    } else {
		    $html = '';
		    foreach ($entries as &$entry) {
			    $html .= \Craft::$app->view->renderTemplate('_includes/filter/overviewItem', array('item' => $entry));
		    }
		    $this->returnData['html'] = $html;

		    $totalPages = ceil($count/$numberOfItems);
		    $this->returnData['pagination'] = \Craft::$app->view->renderTemplate('_includes/filter/pagination', array('totalPages' => $totalPages, 'currentPage' => $pageNr));
	    }
    }

    private function _search($dataArray) {
	    $pageNr = $dataArray->pageNr;
	    $numberOfItems = $dataArray->numberOfItems;
	    $section = $dataArray->section;
	    $fields = $dataArray->fields;

	    $searchString = '';
	    $sliderFields = [];
	    $otherFields = [];

	    $mainLoopCount = count($fields);

	    $offset = $this->_getOffset($pageNr, $numberOfItems);

	    foreach ($fields as &$field) {
	    	if ($field->type == 'number-slider') {
			    array_push($sliderFields, $field);
		    } else {
			    array_push($otherFields, $field);
		    }
	    }


	    foreach ($otherFields as $mainLoopIndex=>&$field) {

		    $subLoopCount = count($field->values);

		    if ($mainLoopCount == 1 && $subLoopCount == 1) {

		    	if ($field->type == 'lightswitch' and $field->values[0] == 0) {
				    $searchString = '-' . $fields[0]->handle . '::1';
			    } else {
				    $searchString = $fields[0]->handle . '::' . $fields[0]->values[0];
			    }

		    } else {

			    if ($mainLoopCount > 1 && $subLoopCount > 1) {
				    $searchString .= '(';
			    }

			    foreach ($field->values as $subLoopIndex=>&$value) {


				    if (($subLoopIndex + 1) == $subLoopCount) {

					    if ($field->type == 'lightswitch' and $value == 0) {
						    $searchString = '-' . $field->handle . '::1';
					    } else {
						    $searchString .= $field->handle . '::' . $value;
					    }

				    } else {
					    $searchString .= $field->handle . '::' . $value . ' OR ';
				    }
			    }

			    if ($mainLoopCount > 1 && $subLoopCount > 1) {
				    $searchString .= ') ';
			    } else {
				    $searchString .= ' ';
			    }
		    }
	    }

	    $searchString = trim($searchString);

	    $query = Entry::find()
		    ->section($section)
		    ->limit($numberOfItems)
		    ->search($searchString)
		    ->offset($offset);

	    foreach ($sliderFields as &$field) {
		    $query->andWhere(['>=', 'field_' . $field->handle, $field->values[0]]);
		    $query->andWhere(['<=', 'field_' . $field->handle, $field->values[1]]);
	    }

	    $count = $query->count();
	    $entries = $query->all();

	    if ($count == 0) {
		    $this->returnData['html'] = \Craft::$app->view->renderTemplate('_includes/filter/noResults');
		    $this->returnData['total'] = 0;

		    $dataType = 'field';
		    $this->returnData['filterTagsHtml'] = $this->_getFilterTagHtml($fields, $dataType);
	    } else {
		    $html = '';
		    foreach ($entries as &$entry) {
			    $html .= \Craft::$app->view->renderTemplate('_includes/filter/overviewItem', array('item' => $entry));
		    }

		    $this->returnData['html'] = $html;
		    $this->returnData['total'] = $count;

		    $dataType = 'field';
		    $this->returnData['filterTagsHtml'] = $this->_getFilterTagHtml($fields, $dataType);

		    $totalPages = ceil($count/$numberOfItems);
		    $this->returnData['pagination'] = \Craft::$app->view->renderTemplate('_includes/filter/pagination', array('totalPages' => $totalPages, 'currentPage' => $pageNr));
	    }
    }

    private function _getOffset($pageNr, $numberOfItems) {
	    $offset = 0;
	    if($pageNr && $pageNr > 1){
		    $offset = ($pageNr - 1) * $numberOfItems;
	    }
	    return $offset;
    }
}
