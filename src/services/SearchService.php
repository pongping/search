<?php
/**
 * Search plugin for Craft CMS 3.x
 *
 * Makes lazy loading Ajax search available
 *
 * @link      https://pongping.studio
 * @copyright Copyright (c) 2019 PongPing
 */

namespace pongping\search\services;

use pongping\search\Search;

use Craft;
use craft\base\Component;

/**
 * SearchService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    PongPing
 * @package   Search
 * @since     1.0.0
 */
class SearchService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Search::$plugin->searchService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
